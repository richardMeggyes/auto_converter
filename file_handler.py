import os
import preferences
from sys import platform


def get_files_and_dirs(path):
    files = []
    dirs = []

    first_iter = True
    for directory in os.walk(path):
        if first_iter:
            first_iter = False
            dirs = directory[1]

        for file in directory[2]:
            file_path = directory[0] + "/" + file
            if check_if_file_is_video(file_path):
                files.append(file_path)

    return files, dirs


def check_if_file_is_video(file):
    ext = file.split(".")[1].lower()
    if ext in preferences.videoformats:
        return True
    else:
        return False


def change_slash(string):
    string.replace("\\\\", "/")
    string.replace("\\", "/")
    string.replace("///", "/")
    string.replace("//", "/")

    if string[len(string) - 1] != "/":
        string += "/"

    if platform == "linux" or platform == "linux2":
        if string[0] != "/":
            string = "/" + string
    return string


def create_dir_if_not_exists(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def create_dirs(path, dirs):
    create_dir_if_not_exists(path)
    for directory in dirs:
        create_dir_if_not_exists(path + "/" + directory)


def get_output_file_path(file_path, input_path, output_path):
    return output_path + "C4_" + file_path.split(input_path)[1]


def get_one_dir_up_path(path):
    path = change_slash(path).split("/")

    new_path = ""
    for x in range(len(path) - 2):
        new_path += path[x] + "/"
    return change_slash(new_path)


def get_broken_file_path(file_path, input_path):
    broken_dir_path = get_one_dir_up_path(input_path) + "BROKEN/"
    create_dir_if_not_exists(broken_dir_path)
    return broken_dir_path + file_path.split(input_path)[1].split("/")[1]


def handle_broken_file(input_file, input_path):
    broken_file_path = get_broken_file_path(input_file, input_path)
    os.rename(input_file, broken_file_path)

    with open(get_one_dir_up_path(input_path) + "BROKEN_FILES.TXT", "a+") as f:
        f.write(input_file + "\n")


def get_file_name_from_path(path):
    path = path.split("/")
    return path[len(path) - 1]
