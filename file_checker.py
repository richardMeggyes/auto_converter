import os
from file_handler import get_file_name_from_path


def file_check(files, output_path):
    error_path = output_path + 'error/'
    error_file_name = 'error.log'
    error_file_path = output_path + error_file_name
    files_num = len(files)
    progress = 0
    error_counter = 0

    for file in files:
        if '.log' not in file and not os.path.isdir(file):
            progress += 1
            video_file_path = file
            os.system('ffmpeg -v error -i "{}" -f null - 2>"{}"'.format(video_file_path, error_file_path))
            error_file_size = os.path.getsize(error_file_path)
            if error_file_size:
                error_counter += 1
                if not os.path.exists(error_path):
                    os.makedirs(error_path)
                os.rename(video_file_path, error_path + get_file_name_from_path(file))
                new_error_file = error_path + get_file_name_from_path(file)[:-4] + '.log'
                os.rename(output_path + error_file_name, new_error_file)
                print('Error found in video file integrity! Moved to error folder.')

            print('Files: {:3}/{:3} Errors: {:2} Error file size: {:5} File: {}'.format(progress, files_num, error_counter,
                                                                                  error_file_size, file))
    #os.remove(error_file_path)
