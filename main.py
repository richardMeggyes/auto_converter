import time
from file_handler import get_files_and_dirs, create_dirs, change_slash
from converter import convert
from file_checker import file_check

if __name__ == "__main__":

    path = "/media/readdeo/UltraBayDrive"

    #input_path = change_slash("e:/TEST/INPUT/")
    #output_path = change_slash("e:/TEST/OUTPUT/")

    input_path = change_slash("/media/readdeo/UltraBayDrive/TEST/INPUT/")
    output_path = change_slash("/media/readdeo/UltraBayDrive/TEST/OUTPUT/")

    input_path = change_slash("/media/readdeo/UltraBayDrive/Training/PUA/INPUT/")
    output_path = change_slash("/media/readdeo/UltraBayDrive/Training/PUA/OUTPUT/")

    files, dirs = get_files_and_dirs(input_path)
    create_dirs(output_path, dirs)

    current_file_number = len(files)

    finished_files = []

    for file in files:
        finished_file = convert(len(files), current_file_number, file, input_path, output_path)
        current_file_number -= 1

        if finished_file != "FAILED":
            finished_files.append(finished_file)

    print("Starting integrity checks")
    time.sleep(3)
    file_check(files, output_path)
