import os, sys, subprocess, re, time
from preferences import max_width, FFmpeg, FFmpeg_preset, FFmpeg_acodec, FFmpeg_vcodec, FFmpeg_rescale
from terminal_handler import printProgressBar
from file_handler import get_output_file_path, handle_broken_file


def get_video_file_width(file):
    # cmd_FFProbe = "ffprobe -v error -show_entries stream=width -of default=noprint_wrappers=1:nokey=1 \"" + self + "\""
    cmd_mediainfo = "mediainfo '--Inform=Video; %Width%/String%'" + " \"" + file + "\""

    print(cmd_mediainfo)
    video_width = os.popen(cmd_mediainfo).read()
    print(video_width)
    video_width = video_width.split('\n')
    for line in video_width:
        if "Width" in line:
            line = line.split(':')
            line = line[1]
            line = line[1:-7]
            line = line.replace(" ", "")
            return line


def get_frames_count(input_file):
    asd = os.popen('mediainfo --fullscan "{}"'.format(input_file)).readlines()
    for line in asd:
        print("MEDINFO ", line)
        if "Frame count" in line:
            line = line.split(": ")
            return int(line[1].replace(" ", ""))
    print("Failed to get frames count")
    return 0



def resize_video(file):
    # Getting video's width using mediainfo
    get_width = get_video_file_width(file)
    video_file_width = int(get_width)  # Getting width of video file
    print('Width of video file / max width: {}/{}'.format(video_file_width, max_width))
    if int(video_file_width) > max_width:  # Setting output file's width
        print("Resizing video to: {}".format(max_width))
        FFmpeg_rescale = " -vf scale={}:-1 ".format(max_width)
    else:
        print("No resizing needed")
        FFmpeg_rescale = " "
    return FFmpeg_rescale


def convert(files_count, current_file_number, input_file, input_path, output_path):
    failed = False
    i = 0
    output_file_path = get_output_file_path(input_file, input_path, output_path)
    frames_number = get_frames_count(input_file)

    cmd = converter_command(input_file, output_file_path)
    start_time = time.time()
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

    for line in iter(p.stdout.readline, b''):
        line = str(line.rstrip())#[2:-1]
        if "Invalid data found when processing input" in line:
            handle_broken_file(input_file, input_path)
            failed = True
            break

        if "frame=" in line:
            try:
                line = re.sub(' +', ' ', line).replace("= ", "=").split(" ")

                frame = int(line[0].split("=")[1])

                elapsed_time = time.strftime("%H:%M:%S", time.gmtime(time.time() - start_time))

                print_progress(get_fps(line), elapsed_time, current_file_number,
                               files_count - current_file_number, frame, frames_number)
                i += 1

            except Exception as e:
                print(e)
                print(line)
        else:
            if len(line) == 0 and p.poll() is not None:
                break
            print(">>> " + line)
    if failed:
        return "FAILED"
    return output_file_path


def print_progress(fps, elapsed_time, current_file_number, remaining_files, frame, frames_number):
    progressbar = printProgressBar(frame, int(frames_number), length=50)
    r_day, r_hour, r_minute, r_second = get_split_time(get_estimated_time_sec(frames_number, frame, fps))

    processed_line = ("%58s FPS: %5s E: %8s R: %2i:%2s:%2s Files: %4s Finished: %4s" % (progressbar, fps, elapsed_time,
                                                                                        r_hour, r_minute, r_second,
                                                                                        current_file_number,
                                                                                        remaining_files))
    print(processed_line, end="\r")


def converter_command(input_file, output_file):
    return FFmpeg + \
           " \"" + \
           input_file + \
           "\"" + \
           FFmpeg_rescale + \
           FFmpeg_preset + \
           " " + \
           FFmpeg_acodec + \
           " " + \
           FFmpeg_vcodec + \
           " \"" + \
           output_file + \
           "\""


def get_split_time(estimated_time_sec):
    r_day = int(time.strftime("%d", time.gmtime(estimated_time_sec))) - 1
    r_hour = int(time.strftime("%H", time.gmtime(estimated_time_sec))) + (r_day * 24)
    r_minute = time.strftime("%M", time.gmtime(estimated_time_sec))
    r_second = time.strftime("%S", time.gmtime(estimated_time_sec))

    return r_day, r_hour, r_minute, r_second


def get_estimated_time_sec(frames_number, frame, fps):
    try:
        return (float(frames_number) - float(frame)) / fps
    except Exception as e:
        print(e)
        return 1

def get_fps(line):
    fps = line[1].split("=")[1]
    if fps == "0.0": fps = "1.1"
    return float(fps)
